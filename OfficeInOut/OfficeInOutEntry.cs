﻿using MySql.Data.Entity;
using OfficeInOut.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeInOut
{
   public  class OfficeInOutEntry
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("start.");

                var datas = CreateBigData();
                //Insert1Async(datas);
                Insert1(datas);

                Console.WriteLine("done.");
            }
            catch(Exception e)
            {
                Trace.WriteLine(e);
            }
        }

       static List<Sheet> CreateBigData()
        {
            var sheets = new List<Sheet>(3000);
            Enumerable.Range(1, 3000).ToList().ForEach(x =>
            {
                var sheet = new Sheet();
                Enumerable.Range(1, 80).ToList().ForEach(y =>
                {
                    var pt = sheet.GetType().GetProperty("項目" + y);
                    pt.SetValue(sheet, "aaa" + y);
                });
                sheets.Add(sheet);
            });

            return sheets;
        }

       static void Clean()
       {
           using (var ctx = new OfficeContext())
           {
               ctx.Database.ExecuteSqlCommand("truncate table sheets");
           }
       }

       static void Insert1(List<Sheet> datas)
       {
           var total = new Stopwatch();
           var loop = new Stopwatch();
           var save = new Stopwatch();

           total.Start();
           using (var ctx = new OfficeContext())
           {
               //ctx.Configuration.AutoDetectChangesEnabled = false;

               loop.Start();
               //datas.ForEach(data =>
               //{
               //    ctx.Sheet.Add(data);
               //});
               ctx.Sheet.AddRange(datas);
               loop.Stop();

               save.Start();
               ctx.SaveChanges();
               save.Stop();
           }
           total.Stop();

           Console.WriteLine("loop:" + loop.Elapsed);
           Console.WriteLine("save:" + save.Elapsed);
           Console.WriteLine("total:" + total.Elapsed);

           Clean();
       }

       async static void Insert1Async(List<Sheet> datas)
       {
           var total = new Stopwatch();
           var loop = new Stopwatch();
           var save = new Stopwatch();

           total.Start();
           using (var ctx = new OfficeContext())
           {
               ctx.Configuration.AutoDetectChangesEnabled = false;

               loop.Start();
               foreach (var data in datas)
               {
                   ctx.Sheet.Add(data);
               }
               //ctx.Sheet.AddRange(datas);
               loop.Stop();

               save.Start();
               await ctx.SaveChangesAsync();
               save.Stop();
           }
           total.Stop();

           Console.WriteLine("loop:" + loop.Elapsed);
           Console.WriteLine("save:" + save.Elapsed);
           Console.WriteLine("total:" + total.Elapsed);

           Clean();
       }
    }
}
