namespace OfficeInOut.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sheets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        column1 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column2 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column3 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column4 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column5 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column6 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column7 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column8 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column9 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column10 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column11 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column12 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column13 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column14 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column15 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column16 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column17 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column18 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column19 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column20 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column21 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column22 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column23 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column24 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column25 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column26 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column27 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column28 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column29 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column30 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column31 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column32 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column33 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column34 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column35 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column36 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column37 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column38 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column39 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column40 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column41 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column42 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column43 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column44 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column45 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column46 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column47 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column48 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column49 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column50 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column51 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column52 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column53 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column54 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column55 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column56 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column57 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column58 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column59 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column60 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column61 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column62 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column63 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column64 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column65 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column66 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column67 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column68 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column69 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column70 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column71 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column72 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column73 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column74 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column75 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column76 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column77 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column78 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column79 = c.String(maxLength: 100, storeType: "nvarchar"),
                        column80 = c.String(maxLength: 100, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Sheets");
        }
    }
}
