﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeInOut.Entity
{
    public class Sheet
    {
        public int Id { get; set; }

        [MaxLength(100)]
        [Column("column1")]
        public string 項目1 { get; set; }
        [MaxLength(100)]
        [Column("column2")]
        public string 項目2 { get; set; }
        [MaxLength(100)]
        [Column("column3")]
        public string 項目3 { get; set; }
        [MaxLength(100)]
        [Column("column4")]
        public string 項目4 { get; set; }
        [MaxLength(100)]
        [Column("column5")]
        public string 項目5 { get; set; }
        [MaxLength(100)]
        [Column("column6")]
        public string 項目6 { get; set; }
        [MaxLength(100)]
        [Column("column7")]
        public string 項目7 { get; set; }
        [MaxLength(100)]
        [Column("column8")]
        public string 項目8 { get; set; }
        [MaxLength(100)]
        [Column("column9")]
        public string 項目9 { get; set; }
        [MaxLength(100)]
        [Column("column10")]
        public string 項目10 { get; set; }
        [MaxLength(100)]
        [Column("column11")]
        public string 項目11 { get; set; }
        [MaxLength(100)]
        [Column("column12")]
        public string 項目12 { get; set; }
        [MaxLength(100)]
        [Column("column13")]
        public string 項目13 { get; set; }
        [MaxLength(100)]
        [Column("column14")]
        public string 項目14 { get; set; }
        [MaxLength(100)]
        [Column("column15")]
        public string 項目15 { get; set; }
        [MaxLength(100)]
        [Column("column16")]
        public string 項目16 { get; set; }
        [MaxLength(100)]
        [Column("column17")]
        public string 項目17 { get; set; }
        [MaxLength(100)]
        [Column("column18")]
        public string 項目18 { get; set; }
        [MaxLength(100)]
        [Column("column19")]
        public string 項目19 { get; set; }
        [MaxLength(100)]
        [Column("column20")]
        public string 項目20 { get; set; }
        [MaxLength(100)]
        [Column("column21")]
        public string 項目21 { get; set; }
        [MaxLength(100)]
        [Column("column22")]
        public string 項目22 { get; set; }
        [MaxLength(100)]
        [Column("column23")]
        public string 項目23 { get; set; }
        [MaxLength(100)]
        [Column("column24")]
        public string 項目24 { get; set; }
        [MaxLength(100)]
        [Column("column25")]
        public string 項目25 { get; set; }
        [MaxLength(100)]
        [Column("column26")]
        public string 項目26 { get; set; }
        [MaxLength(100)]
        [Column("column27")]
        public string 項目27 { get; set; }
        [MaxLength(100)]
        [Column("column28")]
        public string 項目28 { get; set; }
        [MaxLength(100)]
        [Column("column29")]
        public string 項目29 { get; set; }
        [MaxLength(100)]
        [Column("column30")]
        public string 項目30 { get; set; }
        [MaxLength(100)]
        [Column("column31")]
        public string 項目31 { get; set; }
        [MaxLength(100)]
        [Column("column32")]
        public string 項目32 { get; set; }
        [MaxLength(100)]
        [Column("column33")]
        public string 項目33 { get; set; }
        [MaxLength(100)]
        [Column("column34")]
        public string 項目34 { get; set; }
        [MaxLength(100)]
        [Column("column35")]
        public string 項目35 { get; set; }
        [MaxLength(100)]
        [Column("column36")]
        public string 項目36 { get; set; }
        [MaxLength(100)]
        [Column("column37")]
        public string 項目37 { get; set; }
        [MaxLength(100)]
        [Column("column38")]
        public string 項目38 { get; set; }
        [MaxLength(100)]
        [Column("column39")]
        public string 項目39 { get; set; }
        [MaxLength(100)]
        [Column("column40")]
        public string 項目40 { get; set; }
        [MaxLength(100)]
        [Column("column41")]
        public string 項目41 { get; set; }
        [MaxLength(100)]
        [Column("column42")]
        public string 項目42 { get; set; }
        [MaxLength(100)]
        [Column("column43")]
        public string 項目43 { get; set; }
        [MaxLength(100)]
        [Column("column44")]
        public string 項目44 { get; set; }
        [MaxLength(100)]
        [Column("column45")]
        public string 項目45 { get; set; }
        [MaxLength(100)]
        [Column("column46")]
        public string 項目46 { get; set; }
        [MaxLength(100)]
        [Column("column47")]
        public string 項目47 { get; set; }
        [MaxLength(100)]
        [Column("column48")]
        public string 項目48 { get; set; }
        [MaxLength(100)]
        [Column("column49")]
        public string 項目49 { get; set; }
        [MaxLength(100)]
        [Column("column50")]
        public string 項目50 { get; set; }
        [MaxLength(100)]
        [Column("column51")]
        public string 項目51 { get; set; }
        [MaxLength(100)]
        [Column("column52")]
        public string 項目52 { get; set; }
        [MaxLength(100)]
        [Column("column53")]
        public string 項目53 { get; set; }
        [MaxLength(100)]
        [Column("column54")]
        public string 項目54 { get; set; }
        [MaxLength(100)]
        [Column("column55")]
        public string 項目55 { get; set; }
        [MaxLength(100)]
        [Column("column56")]
        public string 項目56 { get; set; }
        [MaxLength(100)]
        [Column("column57")]
        public string 項目57 { get; set; }
        [MaxLength(100)]
        [Column("column58")]
        public string 項目58 { get; set; }
        [MaxLength(100)]
        [Column("column59")]
        public string 項目59 { get; set; }
        [MaxLength(100)]
        [Column("column60")]
        public string 項目60 { get; set; }
        [MaxLength(100)]
        [Column("column61")]
        public string 項目61 { get; set; }
        [MaxLength(100)]
        [Column("column62")]
        public string 項目62 { get; set; }
        [MaxLength(100)]
        [Column("column63")]
        public string 項目63 { get; set; }
        [MaxLength(100)]
        [Column("column64")]
        public string 項目64 { get; set; }
        [MaxLength(100)]
        [Column("column65")]
        public string 項目65 { get; set; }
        [MaxLength(100)]
        [Column("column66")]
        public string 項目66 { get; set; }
        [MaxLength(100)]
        [Column("column67")]
        public string 項目67 { get; set; }
        [MaxLength(100)]
        [Column("column68")]
        public string 項目68 { get; set; }
        [MaxLength(100)]
        [Column("column69")]
        public string 項目69 { get; set; }
        [MaxLength(100)]
        [Column("column70")]
        public string 項目70 { get; set; }
        [MaxLength(100)]
        [Column("column71")]
        public string 項目71 { get; set; }
        [MaxLength(100)]
        [Column("column72")]
        public string 項目72 { get; set; }
        [MaxLength(100)]
        [Column("column73")]
        public string 項目73 { get; set; }
        [MaxLength(100)]
        [Column("column74")]
        public string 項目74 { get; set; }
        [MaxLength(100)]
        [Column("column75")]
        public string 項目75 { get; set; }
        [MaxLength(100)]
        [Column("column76")]
        public string 項目76 { get; set; }
        [MaxLength(100)]
        [Column("column77")]
        public string 項目77 { get; set; }
        [MaxLength(100)]
        [Column("column78")]
        public string 項目78 { get; set; }
        [MaxLength(100)]
        [Column("column79")]
        public string 項目79 { get; set; }
        [MaxLength(100)]
        [Column("column80")]
        public string 項目80 { get; set; }
    }
}
