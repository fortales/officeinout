﻿using MySql.Data.Entity;
using OfficeInOut.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeInOut
{
    public class OfficeContext : DbContext
    {
        public DbSet<Sheet> Sheet { get; set; }
    }
}
